import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import logo from '../logo.svg'

const styles = theme => ({
    content: {

    },
    bigAvatar: {
        width: 80,
        height: 80,
        margin : "0 auto"
    },
    typo : {
        color : theme.palette.secondary.textSecondary
    }
});
const data = {

};

class Account extends React.Component {
    render() {
        return (
            <div className={this.props.classes.content}>
                <Avatar src={logo} className={this.props.classes.bigAvatar}/>
                <Typography variant={"h5"} align={"center"} className={this.props.classes.typo}>
                    User Name
                </Typography>
            </div>
        );
    }
}

export default withStyles(styles)(Account);