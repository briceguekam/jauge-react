// @flow
import React, {Component} from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Jauge from "./Jauge";
import Messages from "./Messages";
import Typography from "@material-ui/core/es/Typography/Typography";


function Message(radius, value, round) {
  this.radius = radius;
  this.value = value;
  this.round = round;
}

let messages = [
  new Message(127, "Troisieme message", 1),
  new Message(67, "Bonjour A la ligne", 1),
  new Message(42, "Hello", 1),
];

let defaultMessage = "Bonjour tout le monde";
const styles = theme => ({
  parent: {
    position: "fixed",
    width: "100%",
    height: "100%",
    paddingTop: "20%",
    left: 0,
    top: 0
  },
  left: {
    display: "inline-block",
    verticalAlign: "top"
  },
  right: {
    display: "inline-block",
    verticalAlign: "top"

  },
  middle: {
    display: "inline-block",
    width: "50%",
    verticalAlign: "top",
  }
});

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      radius: 0,
      round: 0,
      message: "Hello world",
      play: false,
      indexMsg: 1000
    };
  }

  handleMeRadiusChange(radius, round) {
    this.setState({
      radius: radius,
      round: round,
      message: this.getMessage(),
    });
  }

  handleNullChange(radius, round) {
    // console.log(this)
  }

  componentDidMount() {
    window.addEventListener("keyup", (event) => {
      this.setState({play: true});
    })
  }

  getMessage() {
    for (let i = 0; i < messages.length; i++) {
      if (messages[i].radius < this.state.radius && this.state.indexMsg > i) {
        defaultMessage = messages[i].value;
        this.setState({play: false});
        this.state.indexMsg = i;

        return messages[i].value;
      }
    }
    return defaultMessage;
  }

  render() {
    return (
      <div className={this.props.classes.parent}>
        <div className={this.props.classes.left}>
          <Jauge play={this.state.play} round={4}
                 onRadiusChange={(radius, round) => this.handleMeRadiusChange(radius, round)}/>
        </div>
        <div className={this.props.classes.middle}>
          <Messages messages={this.state.message}/>
        </div>
        <div className={this.props.classes.right}>
          <Jauge play={this.state.play} round={1}
                 onRadiusChange={(radius, round) => this.handleNullChange(radius, round)}/>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Content);
