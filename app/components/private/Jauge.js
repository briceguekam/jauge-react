// @flow
import React, {Component} from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Typography from "@material-ui/core/es/Typography/Typography";
// feuille de style utiliser pour personaliser le composant Jauge


const styles = theme => ({
  second: {
    position: "absolute",
    left: 0
  },
  percent: {
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  },
  parent: {
    position: "relative"
  }
});

class Jauge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      radius: 0,
      round: 0,
      finish: false
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      100
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    if(this.props.play === true){
      let lastRadius = this.state.radius;
      // this.handleRadiusChange(lastRadius, this.state.round);
      this.props.onRadiusChange(this.state.radius, this.state.round);
      if (this.state.radius % 100 === 0) {
        this.setState({
          round: this.state.round + 1
        })
      }
      if (this.state.radius === this.props.round * 100) {
        clearInterval(this.timerID);
        this.setState({
          finish: true
        })
      } else {
        this.setState({
          radius: lastRadius + 0.5 * this.props.round
        });
      }
    }
  }
  render() {
    return (
      <div className={this.props.classes.parent}>
        <CircularProgress size={200} variant="static" value={100}/>
        <CircularProgress color={"secondary"} size={200} className={this.props.classes.second} variant="static"
                          value={(this.state.finish) ? 100 : this.state.radius}/>
        <div className={this.props.classes.percent}>
          <Typography variant={"h3"}>
            {(this.state.finish) ? 100 : Math.round(this.state.radius) % 100}%
          </Typography>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Jauge);
